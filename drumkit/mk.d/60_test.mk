# Run our test suite.

.PHONY: tests tests-ci tests-wip tests-js vnc test-steps tests-upstream

tests-complete: tests tests-upstream
tests: ## Run Behat test suite
	$(BEHAT)

tests-upstream:
	$(BEHAT) --stop-on-failure --colors --suite=upstream --tags='~@wip'

tests-ci:
	$(BEHAT) --stop-on-failure --colors --suite=ci

tests-wip:
	$(BEHAT) --tags=wip

tests-js:
	$(BEHAT) --tags=javascript

test-steps:
	$(BEHAT) behat -dl
