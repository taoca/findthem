---
title: Development
weight: 20
---

## INTRODUCTION

Find Them! is still in early development, and relatively untested. It's build
on Drupal 8, and the project is tooled for development with
[Lando](https://lando.dev) and [Drumkit](https://drumk.it).

Soon we should have a proper docs site within this repo, but for now here's
some helpful tips.

GETTING STARTED
---------------

1. Install [Lando](https://docs.lando.dev/basics/installation.html) and
   dependencies. This works best on Linux, YMMV esp on MacOS, where Docker is not great.
1. `git clone https://gitlab.com/taoca/findthem.git ; cd findthem.git`
1. `make all` which is a Drumkit command that does 3 steps at once:
  1. `make start` - spin up Lando containers
  1. `make build` - build the codebase (mainly `composer install`)
  1. `make install` - install a copy of the site

INSTALL PROFILE
---------------

The application is built as a Drupal "distribution" which means it's an [Install
Profile](https://www.drupal.org/docs/distributions/creating-distributions/how-to-write-a-drupal-installation-profile),
which means that a make install will do all the initial setup of the site,
including (as it stands) hardcoding some initial users, which just makes
deployment easy while we're in early alpha stage.


CONFIG MANAGEMENT
-----------------

For the moment, config management is somewhat manual, starting from a fresh
install, making the changes, then doing:

```
lando drush cex -y
git diff config
```

Then scanning through the output for changes to capture in the profile, after
which:

1. copy the target config from `config/$target.yml` to `web/profiles/custom/findthem/config/install`
1. edit the new file under `web/profiles` to remove the `uuid` and `default_config_hash' lines
1. `git add` the resulting files, and commit

In short order, I'd like to move to using
[config_enforce](https://drupal.org/project/config_enforce) to manage config
deployments to production, and
[config_enforce_devel](https://drupal.org/project/config_enforce_devel) to ease
the development workflow.

BEHAT
-----

As soon as possible, we should have some basic Behat scenarios wrapping the
core functionality of the app, and some GitLab CI tooling to enable automatic
testing of new commits.

DOCS
----

The project self-contains this [Hugo](https://gohugo.io) docs site
within the project repository to capture docs for using the project.
