---
title: Operations
weight: 30
---

## Operating Find Them!

DOCKERIZING: if you spin the site up locally with Lando (as desribed in
DEVELOPERS.md), you can then look in ~/.lando/compose/findthem for the set of
Docker Compose docs it generates

- persist database volume
- persist private volume
- ensure outbound email works

## Deploying

1. git clone --recusrive <project-url>
2. composer install
3. settings.local.php

Uncomment these lines at the bottom of `settings.php`:
```
 if (file_exists($app_root . '/' . $site_path . '/settings.local.php')) {
   include $app_root . '/' . $site_path . '/settings.local.php';
 }
```

`settings.local.php`:
```
<?php

$databases['default']['default'] = array (
  'database' => 'drupal8',
  'username' => 'drupal8',
  'password' => 'drupal8',
  'prefix' => '',
  'host' => 'database',
  'port' => '',
  'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
  'driver' => 'mysql',
);
$settings['config_sync_directory'] = '../config';
$settings['file_private_path'] = '/app/private/files';
$settings['file_temp_path'] = '/app/tmp';
$settings['trusted_host_patterns'] = [
  '^findthem.lndo.site$',
];
```
4. Install: a `make install` with Drumkit bootstrapped does these steps, which
   you can do manually or via a deploy script:

```
export SITE_URL=findthem.tao.ca
export SITE_NAME="Find Them!"
export MYSQL_USER=<dbuser>
export MYSQL_PASSWORD=<dbpass>
export MYSQL_DATABASE=<dbname>
export ADMIN_USER=admin
export ADMIN_PASS=<initial-pass>

drush site:install findthem \
                       --site-name=$(SITE_NAME) \
                       --yes --locale="en" \
                       --db-url="mysql://$(MYSQL_USER):$(MYSQL_PASSWORD)@database/$(MYSQL_DATABASE)" \
                       --sites-subdir=$(SITE_URL) \
                       --account-name="$(ADMIN_USER)" \
                       --account-mail="admin@$(SITE_URL)" \
                       --account-pass="$(ADMIN_PASS)"
drush import-taxonomies --choice safe
```

## Updates

- drush cim? (probably unnecessary)
- drush up
