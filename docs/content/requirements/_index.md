---
title: Requirements
weight: 10
---

1. Community organizers should be able to view a map of the "activity" data,
   with the goals of:

* see where in town things are happening
* understand any similarities among data points
* correlate other pieces of information gathered?

2. Community members should be able to submit an activity report, using a data
   entry form:

* map location / address
* photo(s) of the incident
* date/time
* action taken
* people involved
* notes
* tags (free)
* category (curated)

3. Admin users should be able to review submitted activity reports and approve
   them before being published to the map.

4. User Admin users should be able to review and approve user registrations,
   and cancel or ban users as necessary.
