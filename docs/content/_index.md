---
title: "Find Them!"
date: 2020-08-19T02:00:18-04:00
---

# Adversary Activity Tracker (aka Find Them!)

Find Them! is a software tool inspired to coordinate efforts by a local
community to track antisocial activity in a community, be it fascist, racist,
sexist, transphobic, or otherwise oppressive, and support organizing against
such activity and the people perpetrating it.

## GOALS

1. Ingest reports of "activities" with a map location, timestamp, and other notes.
2. Provide a map view of this data, as well as basic search/filtering abilities.
3. Facilitate data analysis for details to understand who is involved and their
   motivations, by identifying themes, types of slogans, or other patterns that may emerge.

KEY GOAL: Identify people engaging in these activities, to help organize community efforts to get rid of them.

## Opensource

This is an opensource project supported by [tao.ca](https://tao.ca), built
with [Drupal](https://drupal.org) using [Drumkit](https://drumk.it) and hosted
at [GitLab](https://gitlab.com/taoca/findthem).

See [Development](dev) for details on how to work with this code repository,
and [Operations](ops) for details on how to operate it.
