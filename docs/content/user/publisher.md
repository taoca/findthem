---
title: Publisher Workflows
weight: 20
---

1. Review & Approve unpublished/recent Reports
2. View Map of Reports
3. Search and Filter report data
