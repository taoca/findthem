---
title: Reporter Workflows
weight: 10
---

## 1. Register your account

Anyone is free to register for an account on Find Them! New users will be
vetted by an Administrator before activation. You will receive an email
notification upon requesting an account, as well as once it has been approved.

* Go to https://findthem.tao.ca/ and select **Create an Account**.
* Provide an **email** and select a **username**, and click Create an Account.
* You will receive an email confirming that you've registered an account, and
  that it is not yet activated.
* You will receive **a second email** in a day or two when an **Administrator has
  approved your account**.
* From this *second* email, click on the **activation link** to set a password.

## 2. File a Report

* Go to https://findthem.tao.ca/ and login with your username and password.
* Select **File a Report**, and select a Location:
  * Type an address in the top right of the map to zoom in on a location
  * Click or Touch a point on the map to drop a pin at a precise spot

![Select a location](/images/file-report-location.png)

* Provide further details of your report:
  * **Short description** of what you saw
  * **Date and time** you noticed it
  * Any **action you took**, such as covering it up/taking it down
  * Notes about **WHO, WHAT or WHEN** that may be relevant
  * Keyword **tags** pertaining to patterns etc
  * **Photos** you took
* Click Save to submit your report.

Upon submission, your post will be unpublished, meaning it can only be viewed
by yourself and Publishers. Once a Publisher approves your report it will go
into the general pool of data for analysis.

## 3. Review your Reports

* Go to https://findthem.tao.ca/my-reports to see a listing of the reports
  you've filed.
* Click on the Title (Short Description) to view the full details
* Click the Edit link to provide updates or new information as needed
