---
title: Administrator Workflows
weight: 30
---

1. Review & Approve new user accounts
2. Cancel abusive/spam accounts
3. Ban IP addresses
