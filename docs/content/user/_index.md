---
title: User Docs
weight: 0
---

Find Them! is a simple data collection/analysis tool with location as its
focus. The key functions are:

1. File a Report - submit info about a particular location
2. Visual Analysis - map view and filtering/searching tools

The following pages contain usage documentation for the primary user personas:

* [Reporter](reporter) - vetted by an admin, simply able to file reports and review/edit their own
* [Publisher](publisher) - empowered to approve/decline reports before publication to the
* [Administrator](admin) - main workflow is to review user registrations, vet and approve new Reporters.
