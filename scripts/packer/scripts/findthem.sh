#!/bin/bash

# Add any necessary steps for setting up findthem inside a CI docker image at Packer image creation time.

# Run a composer install to pre-populate its cache, which should speed up the process in CI.
cd /var/www/findthem
. d
make build VERBOSE=1
rm -rf /var/www/findthem
