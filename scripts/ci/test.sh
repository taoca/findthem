#!/bin/bash

# Test script to be run in CI.

cd /var/www/findthem
. d
make tests
[ -z "$MATRIX_ROOM" ] || make -n -s matrix-ci NOTIFY_CI_RESULT=$?
