#!/bin/bash

# Setup steps to be run inside CI docker image at CI time.

rm -rf /var/www/findthem # TODO: remove from image
ln -s `pwd` /var/www/findthem
cd /var/www/findthem

# Bootstrap Drumkit.
. d
# Build & install the site.
make build install

# Make our docroot readable/executable by apache.
chown -R www-data ./web

# Create private folder
mkdir -p private/files
chown -R www-data private

# Setup local settings file
echo 'if (file_exists($app_root . "/" . $site_path .  "/settings.local.php")) { include $app_root . "/" . $site_path .  "/settings.local.php"; }' >> web/sites/findthem.lndo.site/settings.php
cp ./scripts/ci/settings.local.php web/sites/findthem.lndo.site/

# Deploy our vhosts.
cp scripts/ci/findthem.lndo.site.conf /etc/apache2/sites-available/
ln -s /etc/apache2/sites-available/findthem.lndo.site.conf /etc/apache2/sites-enabled/findthem.lndo.site.conf

# Make our base URL (and chromedriver) resolve.
echo `hostname -I` findthem.lndo.site chromedriver >> /etc/hosts

# Start the web server
service apache2 start

# Check that a site has been installed.
[ -d web/sites/findthem.lndo.site ]
./vendor/bin/drush --uri=findthem.lndo.site status
