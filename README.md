Adversary Activity Tracker (aka Find Them!)
-------------------------------------------

Find Them! is a software tool inspired to coordinate efforts by a local
community to track antisocial activity in a community, be it fascist, racist,
sexist, transphobic, or otherwise oppressive, and support organizing against
such activity and the people perpetrating it.

See https://taoca.gitlab.io/findthem for details.
