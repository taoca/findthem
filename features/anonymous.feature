@anonymous
Feature: Anonymous users should not be able to see any content.
  In order to ensure the security and privacy of submitted data
  As an Anonymous user
  I should not be able to access any data on the site.

  Scenario: Check for access denied on key pages.
    Given I am not logged in
     When I go to "/"
     Then I should see "Find Them!"
      And I should see "Adversary Activity Tracker"
      And I should see "Log in"
      And I should see "Create new account"
      And I should see "Reset your password"
