<?php
/**
 * @file
 * Find Them install profile custom code.
 */

use Drupal\Core\Form\FormStateInterface;


function findthem_form_node_activity_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  $form['#title'] = t('File a Report');
}
