include .mk/GNUmakefile
all: start build install

dev: install dev-enable

dev-enable:
	$(DRUSH) en -y findthem_dev
